from hashlib import sha512
from Crypto.PublicKey import RSA


class RSA_DS:

    def __init__(self, init_bits):
        self.keyPair = RSA.generate(init_bits)

        self.n = self.keyPair.n
        self.e = self.keyPair.e
        self.d = self.keyPair.d

        self.msg_signture = "msg_signture"  # change to verify afterwards

    def sign_scheme(self):
        # RSA sign the message
        hash = int.from_bytes(
            sha512(self.msg_signture.encode('utf-8')).digest(), byteorder='big')
        signature = pow(hash, self.d, self.n)
        return signature

    def verify_scheme(self, msg):

        # RSA verify signature
        hash = int.from_bytes(
            sha512(msg.encode('utf-8')).digest(), byteorder='big')
        hashFromSignature = pow(self.sign_scheme(), self.e, self.n)
        result = True if hash == hashFromSignature else False

        return result


def main():
    bits = int(input("Enter bits ( >= 1024 ): "))
    msg = input("Enter message to verify: ")
    x = RSA_DS(bits)
    print("n = {}\ne = {}\nd = {}".format(x.n, x.e, x.d))
    print("Signture: ", x.sign_scheme())
    # self.msg_signture ="msg_signture"
    print("Signture is valid: ", x.verify_scheme(msg))


if __name__ == "__main__":
    main()
